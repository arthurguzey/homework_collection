package com.java.homework_collection.array;

import com.java.homework_collection.IListGeneric;

import java.util.NoSuchElementException;

public class AList2<T> implements IListGeneric<T> {
    private T[] array;
    private static final int DEFAULT_CAPACITY = 10;
    private int capacity = DEFAULT_CAPACITY;
    private int size=0;

    public AList2(T[] array) {
        this.array = array;
        size = array.length;
        capacity = array.length;
    }

    public AList2(int capacity) {
        if (capacity < 0)
        {
            throw new IndexOutOfBoundsException();
        }
        this.capacity = capacity;
        array = (T[]) new Object[this.capacity];
    }

    public AList2() {
        array = (T[]) new Object[this.capacity];
    }


    @Override
    public void clear() {
        size=0;
        array = null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        if (index>=size || index<0){
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    @Override
    public boolean add(T value) {
        if(size>=capacity)
        {
            array = createArrayCapacity();
        }
        array[size]=value;
        size++;
        return true;
    }

    @Override
    public boolean add(int index, T value) {
        if(index >= size || index < 0)
        {
            return false;
        }
        if(size>=capacity)
        {
            array = createArrayCapacity();
        }
        for(int i = index; i<size; i++)
        {
            array[i+1]=array[i];
        }
        array[index]=value;
        size++;
        return true;
    }

    @Override
    public T remove(T value) {
        int indexOfRemoved = -1;
        for (int i = 0; i < size; i++)
        {
            if (array[i].equals(value))
            {
                indexOfRemoved=i;
            }
        }
        if (indexOfRemoved != -1)
        {
            array[indexOfRemoved]=null;
            for (int i = indexOfRemoved; i < size; i++)
            {
                array[i] = array[i+1];
            }
            array[size-1]=null;
            size--;
            return value;
        }
        else
        {
            throw new NoSuchElementException();
        }
    }

    @Override
    public T removeByIndex(int index) {
        if (index<size && index>=0)
        {
            T removedValue = array[index];
            array[index]=null;
            for (int i = index; i < size; i++)
            {
                array[i] = array[i+1];
            }
            array[size-1]=null;
            size--;
            return removedValue;
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public boolean contains(T value) {
        for (int i = 0; i < size; i++)
        {
            if (value.equals(array[i]))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean set(int index, T value) {
        if(index>=size || index<0)
        {
            return false;
        }
        array[index]=value;
        return true;
    }

    @Override
    public void print() {
        System.out.println("[");
        for(int i=0;i<size;i++)
        {
            System.out.print(array[i]+", ");
        }
        System.out.println("}");
    }

    @Override
    public T[] toArray() {
        T[] returnArray = (T[]) new Object[size];
        for(int i=0;i<size;i++)
        {
            returnArray[i]= array[i];
        }
        return returnArray;
    }

    @Override
    public boolean removeAll(T[] ar) {
        if(ar == null)
        {
            return false;
        }
        for (int i=0; i<ar.length; i++)
        {
            try {
                remove(ar[i]);
            }
            catch (IndexOutOfBoundsException e) {}

        }
        return true;
    }

    private T[] createArrayCapacity(){
        capacity = (capacity*3)/2+1;
        T[] newArray = (T[]) new Object[capacity];
        newArray = array;
        return newArray;
    }
}
