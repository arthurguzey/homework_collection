package com.java.homework_collection.array;

import com.java.homework_collection.IList;

import java.util.NoSuchElementException;

public class AList1 implements IList {
    private int[] array;
    private static final int DEFAULT_CAPACITY = 10;
    private int capacity = DEFAULT_CAPACITY;
    private int size=0;

    public AList1(int[] array) {
        this.array = array;
        size = array.length;
        capacity = array.length;
    }

    public AList1(int capacity) {
        if (capacity < 0)
        {
            throw new IndexOutOfBoundsException();
        }
        this.capacity = capacity;
        array = new int[capacity];
    }

    public AList1() {
        array = new int[capacity];
    }

    @Override
    public void clear() {
        size=0;
        array = null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int get(int index) {
        if (index>=size || index<0){
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    @Override
    public boolean add(int value) {
        if(size>=capacity)
        {
            array = createArrayCapacity();
        }
        array[size]=value;
        size++;
        return true;
    }

    @Override
    public boolean add(int index, int value) {
        if(index>=size || index<0)
        {
            return false;
        }
        for(int i = index; i<size-1; i++)
        {
            if(size>=capacity)
            {
                array = createArrayCapacity();
            }
            array[i+1]=array[i];
        }
        array[index]=value;
        size++;
        return true;
    }

    @Override
    public int remove(int number) {
        int indexOfRemoved = -1;
        for (int i = 0; i < size; i++)
        {
            if (array[i] == number)
            {
                indexOfRemoved=i;
            }
        }
        if (indexOfRemoved != -1)
        {
            array[indexOfRemoved]=0;
            for (int i = indexOfRemoved; i < size; i++)
            {
                array[i] = array[i+1];
            }
            array[size-1]=0;
            size--;
            return number;
        }
        else
        {
            throw new NoSuchElementException();
        }
    }

    @Override
    public int removeByIndex(int index) {
        if (index<size && index>=0)
        {
             int removedValue  = array[index];
            array[index]=0;
            for (int i = index; i < size; i++)
            {
                array[i] = array[i+1];
            }
            array[size-1]=0;
            size--;
            return removedValue;
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public boolean contains(int value) {
        for (int i = 0; i < size; i++)
        {
            if (value == array[i])
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean set(int index, int value) {
        if(index>=size || index<0)
        {
            return false;
        }
        array[index]=value;
        return true;
    }

    @Override
    public void print() {
        System.out.println("[");
        for(int i=0;i<size;i++)
        {
            System.out.print(array[i]+", ");
        }
        System.out.println("}");
    }

    @Override
    public int[] toArray() {
        int[] returnArray = new int[size];
        for(int i=0;i<size;i++)
        {
            returnArray[i]= array[i];
        }
        return returnArray;
    }

    @Override
    public boolean removeAll(int[] ar) {
        if(ar == null)
        {
            return false;
        }
        for (int i=0; i<ar.length; i++)
        {
            try{
                remove(ar[i]);
            }
            catch (NoSuchElementException e){}
        }
        return true;
    }

    private int[] createArrayCapacity(){
        capacity = (capacity*3)/2+1;
        int[] newArray = new int[capacity];
        newArray = array;
        return newArray;
    }
}
