package test.java.homework_collection.array;

import com.java.homework_collection.IList;
import com.java.homework_collection.IListGeneric;
import com.java.homework_collection.array.AList2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.NoSuchElementException;

public class AList2Test {
    private final IListGeneric<String> cut = new AList2<>();

    static Arguments[] toArrayTestArgs() {
        return new Arguments[]{
                Arguments.arguments(2, new String[]{"0", "1"}),
                Arguments.arguments(5, new String[]{"0", "1", "2", "3", "4"}),
                Arguments.arguments(0, new String[]{}),
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(int countAdd, String[] expected) {
        for (int i = 0; i < countAdd; i++) {
            cut.add(String.valueOf(i));
        }

        Object[] actual = cut.toArray();

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void clearTest() {
        int expSize = 0;
        cut.add("A");
        cut.add("B");
        cut.add("C");
        cut.add("D");

        cut.clear();

        Assertions.assertEquals(expSize, cut.size());
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> cut.get(0));
    }

    static Arguments[] addIndexTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 1, "AAA"),
                Arguments.arguments(5, 4, "AAA"),
                Arguments.arguments(5, 1, null),

        };
    }

    @ParameterizedTest
    @MethodSource("addIndexTestArgs")
    void addIndexTest(int countOfAdd, int index, String expected) {

        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }

        cut.add(index, expected);
        String actual = cut.get(index);

        Assertions.assertEquals(expected, actual);
        Assertions.assertEquals(countOfAdd + 1, cut.size());
    }



    static Arguments[] containsTestArgs() {
        return new Arguments[]{
                Arguments.arguments(true, "2", 5),
                Arguments.arguments(true, "6", 5),
                Arguments.arguments(false, "0", 0),
                Arguments.arguments(false, null, 0),
        };
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(boolean expected, String value, int countOfAdd) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(null);
            cut.add(value);
        }

        boolean actual = cut.contains(value);

        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] removeByIndexTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 1),
                Arguments.arguments(5, 4),
                Arguments.arguments(5, 0),
        };
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(int countOfAdd, int index) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }
        int expSize = cut.size() - 1;
        String expRemovedValue = String.valueOf(index);


        String actRemovedValue = cut.removeByIndex(index);
        int actSize = cut.size();


        Assertions.assertEquals(expRemovedValue, actRemovedValue);
        Assertions.assertEquals(expSize, actSize);
    }

    static Arguments[] removeByIndexExceptionTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 6),
                Arguments.arguments(5, -1),
        };
    }

    @ParameterizedTest
    @MethodSource("removeByIndexExceptionTestArgs")
    void removeByIndexExceptionTest(int countOfAdd,  int index) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }

        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> cut.removeByIndex(index));
    }

    @Test
    void removeByIndexNullTest() {
        cut.add("0");
        cut.add(null);
        cut.add("1");
        int expSize = cut.size() - 1;
        String expRemovedValue = null;

        String actRemovedValue = cut.removeByIndex(1);
        int actSize = cut.size();

        Assertions.assertEquals(expRemovedValue, actRemovedValue);
        Assertions.assertEquals(expSize, actSize);
    }

    static Arguments[] removeTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, "1"),
                Arguments.arguments(5, "4"),
        };
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(int countOfAdd, String value) {

        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }

        String actRemovedValue = cut.remove(value);

        Assertions.assertEquals(value, actRemovedValue);
    }

    @Test
    void removeExceptionTest() {

        Assertions.assertThrows(NoSuchElementException.class, () -> cut.remove("AAA"));

        cut.add("BBB");

        Assertions.assertThrows(NoSuchElementException.class, () -> cut.remove("AAA"));
    }

    static Arguments[] setTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 1, "AAA", true),
                Arguments.arguments(5, 0, "AAA", true),
                Arguments.arguments(5, 1, null, true),
        };
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int countOfAdd, int index, String value, boolean expected) {
        cut.add(null);
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }
        int expSize = cut.size();
        boolean actualRez = cut.set(index, value);


        String actual = cut.get(index);
        int actSize = cut.size();

        Assertions.assertEquals(expSize, actSize);
        Assertions.assertEquals(value, actual);
        Assertions.assertEquals(expected, actualRez);
    }

    @Test
    void setFalseTest() {

        Assertions.assertFalse(cut.set(2, "AAA"));

        cut.add("Z");
        cut.add("X");
        cut.add("C");

        Assertions.assertFalse(cut.set(3, "AAA"));
    }

    static Arguments[] removeAllTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, new String[]{"3", "1"}, new String[]{null, "0", "2", "4"}),
                Arguments.arguments(5, new String[]{null, "1"}, new String[]{"0", "2", "3", "4"}),
                Arguments.arguments(5, new String[]{"6", "1"}, new String[]{null, "0", "2", "3", "4"}),
                Arguments.arguments(5, new String[]{"6", "99"}, new String[]{null, "0", "1", "2", "3", "4"}),
                Arguments.arguments(5, new String[]{null, null, "99"}, new String[]{"0", "1", "2", "3", "4"}),
                Arguments.arguments(5, new String[]{}, new String[]{null, "0", "1", "2", "3", "4"}),
                Arguments.arguments(5, null, new String[]{null, "0", "1", "2", "3", "4"}),
        };
    }

    @ParameterizedTest
    @MethodSource("removeAllTestArgs")
    void removeAllTest(int countOfAdd, String[] removeValues, String[] expected) {
        cut.add(null);
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }

        cut.removeAll(removeValues);

        Assertions.assertArrayEquals(expected, cut.toArray());
    }

    @Test
    void removeAllRepeatedValueTest() {
        cut.add("A");
        cut.add("B");
        cut.add("B");
        cut.add("C");
        String[] expected = new String[]{ "B", "C"};

        cut.removeAll(new String[]{"A", "B"});

        Assertions.assertArrayEquals(expected, cut.toArray());
    }
}
