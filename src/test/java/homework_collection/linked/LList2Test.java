package test.java.homework_collection.linked;

import com.java.homework_collection.IListGeneric;
import com.java.homework_collection.linked.LList2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class LList2Test {
    private final IListGeneric<String> cut = new LList2<>();

    static Arguments[] constructorsTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new LList2<String>(), new String[]{}, 0),
                Arguments.arguments(new LList2<String>(new String[]{"1", null, "2"}), new String[]{"1", null, "2"}, 3),
        };
    }


    @ParameterizedTest
    @MethodSource("constructorsTestArgs")
    void constructorTest(IListGeneric<String> cut, String[] expected, int expSize) {
        Object[] actual = cut.toArray();
        int actSize = cut.size();

        Assertions.assertEquals(expSize, actSize);
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void constructorsExceptionTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new LList2<String>(null));
    }

    static Arguments[] toArrayAndAddTestArgs() {
        return new Arguments[]{
                Arguments.arguments(2, new String[]{"0", "1"}),
                Arguments.arguments(5, new String[]{"0", "1", "2", "3", "4"}),
                Arguments.arguments(0, new String[]{}),
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayAndAddTestArgs")
    void toArrayAndAddTest(int countAdd, String[] expected) {
        for (int i = 0; i < countAdd; i++) {
            cut.add(String.valueOf(i));
        }

        Object[] actual = cut.toArray();

        Assertions.assertArrayEquals(expected, actual);
    }


    static Arguments[] getTestArgs() {
        return new Arguments[]{
                Arguments.arguments(1),
                Arguments.arguments(5),
                Arguments.arguments(12)
        };
    }

    @ParameterizedTest
    @MethodSource("getTestArgs")
    void getTest(int countOfAdd) {
        String expected = "";
        for (int i = 0; i < countOfAdd; i++) {
            expected = String.valueOf(i);
            cut.add(expected);
        }

        String actual = cut.get(cut.size() - 1);

        Assertions.assertEquals(expected, actual);
        Assertions.assertEquals(countOfAdd, cut.size());
    }

    static Arguments[] getExceptionTestArgs() {
        return new Arguments[]{
                Arguments.arguments(0, 0),
                Arguments.arguments(0, 1),
                Arguments.arguments(5, 5),
                Arguments.arguments(5, 7),
                Arguments.arguments(5, -1),
        };
    }

    @ParameterizedTest
    @MethodSource("getExceptionTestArgs")
    void getExceptionTest(int countOfAdd, int index) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }

        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> cut.get(index));
    }


    static Arguments[] addIndexTestArgs() {
        return new Arguments[] {
                Arguments.arguments(0, "D", true, new String[]{"A", "B", "C"}, new String[]{"D", "A", "B", "C"}),
                Arguments.arguments(1, "D", true, new String[]{"A", "B", "C"}, new String[]{"A", "D", "B", "C"}),
                Arguments.arguments(2, "D", true, new String[]{"A", "B", "C"}, new String[]{"A", "B", "D", "C"}),
                Arguments.arguments(3, "D", true, new String[]{"A", "B", "C"}, new String[]{"A", "B", "C", "D"}),
                Arguments.arguments(0, "D", true, new String[]{}, new String[]{"D"}),
                Arguments.arguments(1, "D", true, new String[]{"A"}, new String[]{"A", "D"}),
                Arguments.arguments(1, null, true, new String[]{"A"}, new String[]{"A", null}),
        };
    }

    @ParameterizedTest
    @MethodSource("addIndexTestArgs")
    void addIndexTest(int index, String value, boolean expectedRez, String[] oldArr, String[] expected) {
        for (int i = 0; i < oldArr.length; i++) {
            cut.add(oldArr[i]);
        }
        boolean actualRez = cut.add(index, value);
        Object[] actual = cut.toArray();
        Assertions.assertEquals(expectedRez, actualRez);
        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] addIndexTestFalseArgs() {
        return new Arguments[]{
                Arguments.arguments(1, false, new String[]{}),
                Arguments.arguments(-1, false, new String[]{"A", "B"}),
                Arguments.arguments(3, false, new String[]{"A", "B"})
        };
    }

    @ParameterizedTest
    @MethodSource("addIndexTestFalseArgs")
    void addIndexTestFalse(int index, boolean expected, String[] oldArr) {
        for (int i = 0; i < oldArr.length; i++) {
            cut.add(oldArr[i]);
        }

        boolean actual = cut.add(index, "value");

        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] removeTestArgs() {
        return new Arguments[] {
                Arguments.arguments("A", new String[]{"A", "B", "C"}, new String[]{"B", "C"}),
                Arguments.arguments("B", new String[]{"A", "B", "C"}, new String[]{"A", "C"}),
                Arguments.arguments("C", new String[]{"A", "B", "C"}, new String[]{"A", "B"}),
                Arguments.arguments("C", new String[]{"C"}, new String[]{}),

        };
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(String expValue, String[] oldArr, String[] expected) {
        for (int i = 0; i < oldArr.length; i++) {
            cut.add(oldArr[i]);
        }

        String actualValue = cut.remove(expValue);
        Object[] actual = cut.toArray();

        Assertions.assertEquals(expValue, actualValue);
        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] removeNullTestArgs() {
        return new Arguments[] {
                Arguments.arguments("D", new String[]{"A", "B", "C"}, new String[]{"A", "B", "C"}),
                Arguments.arguments(null , new String[]{"A", "B", "C"}, new String[]{"A", "B", "C"}),
                Arguments.arguments(null, new String[]{"A", null, "C"}, new String[]{"A", "C"})
        };
    }

    @ParameterizedTest
    @MethodSource("removeNullTestArgs")
    void removeNull(String expValue, String[] oldArr, String[] expected) {
        for (int i = 0; i < oldArr.length; i++) {
            cut.add(oldArr[i]);
        }

        String actualValue = cut.remove(expValue);
        Object[] actual = cut.toArray();

        Assertions.assertNull(actualValue);
        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] setTestArgs() {
        return new Arguments[] {
                Arguments.arguments(true, 0, "D", new String[]{"A", "B", "C"}, new String[]{"D", "B", "C"}),
                Arguments.arguments(true, 1, "D", new String[]{"A", "B", "C"}, new String[]{"A", "D", "C"}),
                Arguments.arguments(true, 2, "D", new String[]{"A", "B", "C"}, new String[]{"A", "B", "D"}),
                Arguments.arguments(true, 0, null, new String[]{"A", "B", "C"}, new String[]{null, "B", "C"}),
                Arguments.arguments(true, 0, "D", new String[]{null, "B", "C"}, new String[]{"D", "B", "C"}),
                Arguments.arguments(false, 3, "D", new String[]{"A", "B", "C"}, new String[]{"A", "B", "C"}),
                Arguments.arguments(false, -1, "D", new String[]{"A", "B", "C"}, new String[]{"A", "B", "C"}),
                Arguments.arguments(false, 0, "D", new String[]{}, new String[]{}),
        };
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(boolean expected, int index, String value, String[] oldArr, String[] expectedArr) {
        for (int i = 0; i < oldArr.length; i++) {
            cut.add(oldArr[i]);
        }

        boolean actual = cut.set(index, value);
        Object[] actualArr = cut.toArray();

        Assertions.assertEquals(expected, actual);
        Assertions.assertArrayEquals(expectedArr, actualArr);
    }

//    ****************************************************************

    static Arguments[] removeAllTestArgs() {
        return new Arguments[] {
                Arguments.arguments(true, new String[]{"A", "C"}, new String[]{"A", "B", "C", "D"}, new String[]{"B", "D"}),
                Arguments.arguments(true, new String[]{"C", "A"}, new String[]{"A", "B", "C", "D"}, new String[]{"B", "D"}),
                Arguments.arguments(true, new String[]{"X", "C", "A"}, new String[]{"A", "B", "C", "D"},
                        new String[]{"B", "D"}),
                Arguments.arguments(true, new String[]{"X", "Z"}, new String[]{"A", "B", "C", "D"},
                        new String[]{"A", "B", "C", "D"}),
                Arguments.arguments(true, new String[]{"A", "C"}, new String[]{},  new String[]{}),
                Arguments.arguments(false, null, new String[]{"A", "B", "A", "D"},  new String[]{"A", "B", "A", "D"}),
        };
    }

    @ParameterizedTest
    @MethodSource("removeAllTestArgs")
    void removeAllTest(boolean expected, String[] removeArr, String[] oldArr, String[] expectedArr) {
        for (int i = 0; i < oldArr.length; i++) {
            cut.add(oldArr[i]);
        }

        boolean actual = cut.removeAll(removeArr);
        Object[] actualArr = cut.toArray();

        Assertions.assertEquals(expected, actual);
        Assertions.assertArrayEquals(expectedArr, actualArr);
    }
}
